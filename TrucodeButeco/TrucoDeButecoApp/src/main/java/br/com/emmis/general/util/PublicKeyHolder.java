package br.com.emmis.general.util;

import android.content.Context;
import android.util.Base64;

import java.io.IOException;
import java.io.InputStream;

import br.com.emmis.trucodebuteco.R;


/**
 * Created by vaner on 28/04/2015.
 */
public class PublicKeyHolder {

    private static final String sMASK = "KioqIGVNTWkncyBFbmdlbmhhcmlhIGRlIFNvZnR3YXJlICoqKiBWSk0gQUxNTSBNSk1NICoqKg==";
    private byte[] mMask;
    private Context mContext;

    public PublicKeyHolder(Context context) {
        mMask = decodeB64(sMASK);
        mContext = context;
    }

    public byte[] decodeB64(String str) {
        return Base64.decode(str, Base64.NO_WRAP);
    }

    public byte[] decodeB64(byte[] str) {
        return Base64.decode(str, Base64.NO_WRAP);
    }

    public String encodeB64(byte[] str) {
        return Base64.encodeToString(str, Base64.NO_WRAP);
    }

    private byte getMask(int i) {
        int length = mMask.length;
        return mMask[i % length];
    }

    public String generatePublicKey() {

        byte[] publicKeyBytes = new byte[294];
        InputStream publicKey = mContext.getResources().openRawResource(R.raw.public_key);

        try {
            publicKey.read(publicKeyBytes);
        } catch (IOException e) {
            e.printStackTrace();
        }

        int size = publicKeyBytes.length;
        byte[] answer = new byte[size];


        for (int i = 0; i < size; i++) {
            byte mask = getMask(i);
            answer[i] = (byte) (publicKeyBytes[i] ^ mask);
        }

        String debug = encodeB64(answer);

        return debug;
    }
}
