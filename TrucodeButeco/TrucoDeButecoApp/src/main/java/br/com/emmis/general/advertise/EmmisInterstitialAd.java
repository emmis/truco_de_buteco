package br.com.emmis.general.advertise;


import android.app.Activity;

import com.google.android.gms.ads.AdRequest;
import com.purplebrain.adbuddiz.sdk.AdBuddiz;

import br.com.emmis.general.settings.Settings;
import br.com.emmis.trucodebuteco.R;


/**
 * Created by vaner on 18/08/2015.
 */
public class EmmisInterstitialAd {

    private com.google.android.gms.ads.InterstitialAd mAdmobInterstitial;
    private Activity mContext;
    private int mAlternateInterstitial;
    private boolean mEnable;
    private static boolean LOADED = false;
    private Settings mSettings;

    public static EmmisInterstitialAd getInstance(Activity ctx) {
        EmmisInterstitialAdHolder.mInterstitial.setmContext(ctx);
        if (!LOADED) {
            EmmisInterstitialAdHolder.mInterstitial.load();
        }
        return EmmisInterstitialAdHolder.mInterstitial;
    }

    private void setmContext(Activity mContext) {
        this.mContext = mContext;
    }

    private EmmisInterstitialAd() {
    }

    private void load() {
        mSettings = Settings.getInstance(mContext);

        LOADED = true;
        mAlternateInterstitial = 0;

        /**
         * TODO: In production, enable ads!
         */
        mEnable = false;

        if(mEnable) {
            AdBuddiz.setPublisherKey(mContext.getString(R.string.ad_buddiz_id).toString());

            // Criar o anúncio intersticial.
            mAdmobInterstitial = new com.google.android.gms.ads.InterstitialAd(mContext);
            mAdmobInterstitial.setAdUnitId(mContext.getString(R.string.interstitial_ad_unit_id));

            AdBuddiz.cacheAds(mContext);

            if (mSettings.ismEnableSound()) {
                AdBuddiz.RewardedVideo.fetch(mContext); // this = current Activity
            }

            requestNewAdmobInterstitial();

            mAdmobInterstitial.setAdListener(new com.google.android.gms.ads.AdListener() {
                @Override
                public void onAdClosed() {
                    requestNewAdmobInterstitial();
                    super.onAdClosed();
                }
            });
        }
    }

    private void requestNewAdmobInterstitial() {
        AdRequest adRequest = new AdRequest.Builder()
			.addTestDevice(AdRequest.DEVICE_ID_EMULATOR)		// EMULADOR
			.addTestDevice("B66A7A15F6140D51373B10B03A786DDD")	// GALAXY Y - VANER
			.addTestDevice("A8C8B27C9DC23CB9E1E87DA0D96A6160")	// LG L30 - VANER
			.build();
        mAdmobInterstitial.loadAd(adRequest);
    }

    public void displayInterstitial() {

        if(mEnable) {
            int retry = 0;
            while (retry < 10) {
                if (mAlternateInterstitial <= 2 && mAdmobInterstitial!= null && mAdmobInterstitial.isLoaded()) {
                    mAdmobInterstitial.show();
                    mAlternateInterstitial++;
                    retry = 10;
                } else if (mAlternateInterstitial == 4 && AdBuddiz.RewardedVideo.isReadyToShow(mContext)) {
                    AdBuddiz.RewardedVideo.show(mContext);
                    mAlternateInterstitial = 0;
                    retry = 10;
                } else if (mAlternateInterstitial == 3 && AdBuddiz.isReadyToShowAd(mContext)) {
                    AdBuddiz.showAd(mContext);
                    mAlternateInterstitial++;
                    retry = 10;
                } else {
                    mAlternateInterstitial++;
                    mAlternateInterstitial%=5;
                    retry++;
                }
            }
        }
    }

    public void setAdEnable(boolean enable) {
        mEnable = enable;
    }

    private static class EmmisInterstitialAdHolder {
        public static EmmisInterstitialAd mInterstitial = new EmmisInterstitialAd();
    }
}
