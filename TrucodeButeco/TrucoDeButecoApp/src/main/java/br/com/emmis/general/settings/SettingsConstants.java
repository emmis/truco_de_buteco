package br.com.emmis.general.settings;

/**
 * Created by vaner on 10/04/2015.
 */
public class SettingsConstants {

    public static final String PREF_ENABLE_SOUND = "pref_sound_enabled";
    public static final String PREF_KEEP_SCREEN_ON = "pref_keep_screen";

    public static final String PREF_ENABLE_GOOGLE_PLAY = "pref_enable_google_play";

}
