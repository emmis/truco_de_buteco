package br.com.emmis.general.advertise;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import br.com.emmis.trucodebuteco.R;


/**
 * Created by vaner on 18/08/2015.
 */
public class EmmisAdsFragment extends Fragment {

    public EmmisAdsFragment() {
        // Required empty public constructor
    }

    /**
     * This class makes the ad request and loads the ad.
     */
    public static class AdsFragment extends Fragment {

        private static final String TAG = "AdFragment";
        private AdView mAdView;
        private RelativeLayout mContainer;

        public AdsFragment() {
        }

        @Override
        public void onActivityCreated(Bundle bundle) {
            super.onActivityCreated(bundle);

            /**
             * TODO: In production, enable ads!
             */
            boolean removeAds = true;
            Context ctx = getActivity();

            if(!removeAds) {
                mAdView = (AdView) getView().findViewById(R.id.adView);

                mAdView.setAdListener(new com.google.android.gms.ads.AdListener() {
                    @Override
                    public void onAdLoaded() {
                        mContainer.setVisibility(View.VISIBLE);
                        Log.d("TAG", "onAdLoaded");
                        super.onAdLoaded();
                    }

                    @Override
                    public void onAdClosed() {
                        mContainer.setVisibility(View.GONE);
                        Log.d("TAG", "onAdClosed");
                        super.onAdClosed();
                    }
                });

                // Create an ad request. Check logcat output for the hashed device ID to
                // get test ads on a physical device. e.g.
                // "Use AdRequest.Builder.addTestDevice("ABCDEF012345") to get test ads on this device."
                AdRequest adRequest = new AdRequest.Builder()
                        .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)		// EMULADOR
                        .addTestDevice("A8C8B27C9DC23CB9E1E87DA0D96A6160")	// GALAXY Y - VANER
                        .addTestDevice("B66A7A15F6140D51373B10B03A786DDD")	// LG L30 - VANER
                        .build();

                // Start loading the ad in the background.
                mAdView.loadAd(adRequest);
            }
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View view = null;

            view = inflater.inflate(R.layout.fragment_admob_ad, container, true);
            mContainer = (RelativeLayout) view.findViewById(R.id.adViewContainer);

            return view;
        }

        /**
         * Called when leaving the activity
         */
        @Override
        public void onPause() {
            if (mAdView != null) {
                mAdView.pause();
            }
            super.onPause();
        }

        /**
         * Called when returning to the activity
         */
        @Override
        public void onResume() {
            super.onResume();
            if (mAdView != null) {
                mAdView.resume();
            }
        }

        /**
         * Called before the activity is destroyed
         */
        @Override
        public void onDestroy() {
            if (mAdView != null) {
                mAdView.destroy();
            }

            super.onDestroy();
        }
    }
}
