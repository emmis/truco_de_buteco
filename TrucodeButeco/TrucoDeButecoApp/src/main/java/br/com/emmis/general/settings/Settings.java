package br.com.emmis.general.settings;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.preference.PreferenceManager;

import java.util.Calendar;


/**
 * Created by vaner on 27/02/2015.
 */
public class Settings {
    private static boolean LOADED = false;
    private Context mContext;
    private boolean mEnableSound;
    private boolean mKeepScreenOn;


    public static br.com.emmis.general.settings.Settings getInstance(Context ctx) {
        ConfigHolder.mSettings.setmContext(ctx);
        if (!LOADED) {
            ConfigHolder.mSettings.loadConfig();
        }
        return ConfigHolder.mSettings;
    }

    private void setmContext(Context mContext) {
        this.mContext = mContext;
    }

    private void loadConfig() {
        LOADED = true;

        // Restore preferences
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(mContext);
        mEnableSound = settings.getBoolean(SettingsConstants.PREF_ENABLE_SOUND, true);
        mKeepScreenOn = settings.getBoolean(SettingsConstants.PREF_KEEP_SCREEN_ON, true);

    }

    public boolean ismEnableSound() {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(mContext);
        mEnableSound = settings.getBoolean(SettingsConstants.PREF_ENABLE_SOUND, true);
        return mEnableSound;
    }

    public boolean ismKeepScreenOn() {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(mContext);
        mKeepScreenOn = settings.getBoolean(SettingsConstants.PREF_KEEP_SCREEN_ON, true);
        return mKeepScreenOn;
    }

    public boolean isGooglePlayEnabled() {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(mContext);
        return settings.getBoolean(SettingsConstants.PREF_ENABLE_GOOGLE_PLAY, true);
    }

    @Override
    public String toString() {
        loadConfig();
        return "{" +
                "Sound=" + mEnableSound +
                ", Screen=" + mKeepScreenOn +
                '}';
    }

    private static class ConfigHolder {
        public static br.com.emmis.general.settings.Settings mSettings = new br.com.emmis.general.settings.Settings();
    }
}
